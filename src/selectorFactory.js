const mapAndReverse = (arr, fn) => {
  return arr.reduceRight(
    (accumulator, currentValue) => [fn(currentValue), ...accumulator],
    [],
  )
}

const selectorFactory = (...args) => {
  const [resultFunction, ...inputSelectors] = args.reverse()
  const memoisedResultFunction = createMemoisedBinaryFunction(resultFunction)
  return (state) =>
    memoisedResultFunction(
      ...mapAndReverse(inputSelectors, (inputSelector) => inputSelector(state)),
    )
}

const createMemoisedBinaryFunction = (binaryFunctionToMemoise) => {
  const cachedResults = new Map()
  const memoisedBinaryFunction = (a, b) => {
    const memoisedResult = cachedResults.get(a)?.get(b)

    if (memoisedResult !== undefined) {
      return memoisedResult
    }

    const result = binaryFunctionToMemoise(a, b)

    const mapping = cachedResults.has(a)
      ? cachedResults.get(a)
      : cachedResults.set(a, new Map()).get(a)

    mapping.set(b, result)
    return result
  }
  return memoisedBinaryFunction
}

export default selectorFactory
